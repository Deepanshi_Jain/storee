

function Footer() {
  return (
    <div >
       <footer className="bg-dark pt-5">
        <div className="px-lg-3 pt-2 pb-4">
          <div className="mx-auto px-3" style= {{maxWidth: "80rem" }} >
            <div className="row">
              <div className="col-xl-2 col-lg-3 col-sm-4 pb-2 mb-4">
                <div className="mt-n1">
                  <a className="d-inline-block align-middle" href="#">
                    <img className="d-block mb-4" width="117" src="img/footer-logo-light.png" alt="Cartzilla"/>
                  </a>
                </div>
                <div className="btn-group dropdown disable-autohide">
                  <button className="btn btn-outline-light border-light btn-sm dropdown-toggle px-2" type="button" data-toggle="dropdown"><img className="mr-2" width="20" src="img/flags/en.png" alt="English"/>Eng / $
                  </button>
                  <ul className="dropdown-menu">
                    <li className="dropdown-item">
                      <select className="custom-select custom-select-sm">
                        <option value="usd">$ USD</option>
                        <option value="eur">â‚¬ EUR</option>
                        <option value="ukp">Â£ UKP</option>
                        <option value="jpy">Â¥ JPY</option>
                      </select>
                    </li>
                    <li><a className="dropdown-item pb-1" href="#"><img className="mr-2" width="20" src="img/flags/fr.png" alt="FranÃ§ais"/>FranÃ§ais</a></li>
                    <li><a className="dropdown-item pb-1" href="#"><img className="mr-2" width="20" src="img/flags/de.png" alt="Deutsch"/>Deutsch</a></li>
                    <li><a className="dropdown-item" href="#"><img className="mr-2" width="20" src="img/flags/it.png" alt="Italiano"/>Italiano</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-4">
                <div className="widget widget-links widget-light pb-2 mb-4">
                  <h3 className="widget-title text-light">Product catalog</h3>
                  <ul className="widget-list">
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Special offers</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Bakery</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Fruits and Vegetables</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Dairy and Eggs</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Meat and Poultry</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Fish and Seafood</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Sauces and Spices</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Canned Food and Oil</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Alcoholic Beverages</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Soft Drinks and Juice</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Packets, Cereals and Poultry</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Frozen</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Personal hygiene</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Kitchenware</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-4">
                <div className="widget widget-links widget-light pb-2 mb-4">
                  <h3 className="widget-title text-light">Company</h3>
                  <ul className="widget-list">
                    <li className="widget-list-item"><a className="widget-list-link" href="#">About us</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Store locator</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Careers at Cartzilla</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Contacts</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Help center</a></li>
                    <li className="widget-list-item"><a className="widget-list-link" href="#">Actions and News</a></li>
                  </ul>
                </div>
                <div className="widget widget-light pb-2 mb-4">
                  <h3 className="widget-title text-light">Follow us</h3><a className="social-btn sb-light sb-twitter mr-2 mb-2" href="#"><i className="czi-twitter"></i></a><a className="social-btn sb-light sb-facebook mr-2 mb-2" href="#"><i className="czi-facebook"></i></a><a className="social-btn sb-light sb-instagram mr-2 mb-2" href="#"><i className="czi-instagram"></i></a><a className="social-btn sb-light sb-youtube mr-2 mb-2" href="#"><i className="czi-youtube"></i></a>
                </div>
              </div>
              <div className="col-xl-4 col-sm-8">
                <div className="widget pb-2 mb-4">
                  <h3 className="widget-title text-light pb-1">Stay informed</h3>
                  <form className="cz-subscribe-form validate" action="https://studio.us12.list-manage.com/subscribe/post?u=c7103e2c981361a6639545bd5&amp;amp;id=29ca296126" method="post" name="mc-embedded-subscribe-form" target="_blank" >
                    <div className="input-group input-group-overlay flex-nowrap">
                      <div className="input-group-prepend-overlay"><span className="input-group-text text-muted font-size-base"><i className="czi-mail"></i></span></div>
                      <input className="form-control prepended-form-control" type="email" name="EMAIL" placeholder="Your email" required/>
                      <div className="input-group-append">
                        <button className="btn btn-primary" type="submit" name="subscribe">Subscribe*</button>
                      </div>
                    </div>
                    <div style= {{position: "absolute" , left: "-5000px"}}  aria-hidden="true">
                      <input className="cz-subscribe-form-antispam" type="text" name="b_c7103e2c981361a6639545bd5_29ca296126" />
                    </div>
                    <small className="form-text text-light opacity-50">*Subscribe to our newsletter to receive early discount offers, updates and new products info.</small>
                    <div className="subscribe-status"></div>
                  </form>
                </div>
                <div className="widget pb-2 mb-4">
                  <h3 className="widget-title text-light pb-1">Download our app</h3>
                  <div className="d-flex flex-wrap">
                    <div className="mr-2 mb-2"><a className="btn-market btn-apple" href="#" role="button"><span className="btn-market-subtitle">Download on the</span><span className="btn-market-title">App Store</span></a></div>
                    <div className="mb-2"><a className="btn-market btn-google" href="#" role="button"><span className="btn-market-subtitle">Download on the</span><span className="btn-market-title">Google Play</span></a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-darker px-lg-3 py-3">
          <div className="d-sm-flex justify-content-between align-items-center mx-auto px-3" style={{ maxWidth: "80rem"}} >
            <div className="font-size-xs text-light opacity-50 text-center text-sm-left py-3">Â© All rights reserved. Made by <a className="text-light" href="https://createx.studio/" target="_blank" rel="noopener">Createx Studio</a></div>
            <div className="py-3"><img className="d-block mx-auto mx-sm-left" width="187" src="img/cards-alt.png" alt="Payment methods"/>
            </div>
          </div>
        </div>
      </footer>
      </div>
      )}

      export default Footer;