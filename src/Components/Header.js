

function Header() {
  return (
    <header className="bg-light box-shadow-sm fixed-top">
      <div className="navbar navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <a
            className="navbar-brand d-none d-sm-block mr-3 mr-xl-4 flex-shrink-0"
            href="index.html"
            style={{ minWwidth: "7rem" }}
          >
            <img width="142" src="img/logo-dark.png" alt="Cartzilla" />
          </a>
          <a
            className="navbar-brand d-sm-none mr-2"
            href="index.html"
            style={{ minWidth: "4.625rem" }}
          >
            <img width="74" src="img/logo-icon.png" alt="Cartzilla" />
          </a>
          {/* <!-- Search--> */}
          < div className="input-group-overlay d-none d-lg-block mx-6">
            <div className="input-group-prepend-overlay">
              <span className="input-group-text">
                <i className="czi-search"></i>
              </span>
              </div>
              <input
                className="form-control prepended-form-control appended-form-control"
                type="text"
                placeholder="Search for products"
              />
             
              <div className="input-group-append-overlay">
                <select className="custom-select">
                  <option>All categories</option>
                  <option>Bakery</option>
                  <option>Fruits and Vegetables</option>
                  <option>Dairy and Eggs</option>
                  <option>Meat and Poultry</option>
                  <option>Fish and Seafood</option>
                  <option>Sauces and Spices</option>
                  <option>Canned Food and Oil</option>
                  <option>Alcoholic Beverages</option>
                  <option>Soft Drinks and Juice</option>
                  <option>Packets, Cereals</option>
                  <option>Frozen</option>
                  <option>Snaks, Sweets and Chips</option>
                  <option>Personal hygiene</option>
                  <option>Kitchenware</option>
                </select>
              </div>
              </div>
          
        </div>
        {/* <!-- Toolbar--> */}
        <div className="navbar-toolbar d-flex flex-shrink-0 align-items-center ml-xl-2">
          <a className="navbar-toggler" href="#sideNav" data-toggle="sidebar">
            <span className="navbar-toggler-icon"></span>
          </a>
          <a
            className="navbar-tool d-flex d-lg-none"
            href="#searchBox"
            data-toggle="collapse"
            role="button"
            aria-expanded="false"
            aria-controls="searchBox"
          >
            <span className="navbar-tool-tooltip">Search</span>
            <div className="navbar-tool-icon-box">
              <i className="navbar-tool-icon czi-search"></i>
            </div>
          </a>
          <a className="navbar-tool d-none d-lg-flex" href="#">
            <span className="navbar-tool-tooltip">Wishlist</span>
            <div className="navbar-tool-icon-box">
              <i className="navbar-tool-icon czi-heart"></i>
            </div>
          </a>
          <a
            className="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2"
            href="#signin-modal"
            data-toggle="modal"
          >
            <div className="navbar-tool-icon-box">
              <i className="navbar-tool-icon czi-user"></i>
            </div>
            <div className="navbar-tool-text ml-n3">
              <small>Hello, Sign in</small>My Account
            </div>
          </a>
          <div className="navbar-tool dropdown ml-3">
            <a
              className="navbar-tool-icon-box bg-secondary dropdown-toggle"
              href="grocery-checkout.html"
            >
              <span className="navbar-tool-label">3</span>
              <i className="navbar-tool-icon czi-cart"></i>
            </a>
            <a className="navbar-tool-text" href="grocery-checkout.html">
              <small>My Cart</small>$25.00
            </a>
            <div
              className="dropdown-menu dropdown-menu-right"
              style={{ width: "20rem" }}
            >
              <div className="widget widget-cart px-3 pt-2 pb-3">
                <div
                  style={{ height: "15rem" }}
                  data-simplebar
                  data-simplebar-auto-hide="false"
                >
                  <div className="widget-cart-item pb-2 border-bottom">
                    <button
                      className="close text-danger"
                      type="button"
                      aria-label="Remove"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <div className="media align-items-center">
                      <a className="d-block mr-2" href="grocery-single.html">
                        <img
                          width="64"
                          src="img/grocery/cart/th01.jpg"
                          alt="Product"
                        />
                      </a>
                      <div className="media-body">
                        <h6 className="widget-product-title">
                          <a href="grocery-single.html">
                            Frozen Oven-ready Poultry
                          </a>
                        </h6>
                        <div className="widget-product-meta">
                          <span className="text-accent mr-2">
                            $15.<small>00</small>
                          </span>
                          <span className="text-muted">x 1</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="widget-cart-item py-2 border-bottom">
                    <button
                      className="close text-danger"
                      type="button"
                      aria-label="Remove"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <div className="media align-items-center">
                      <a className="d-block mr-2" href="grocery-single.html">
                        <img
                          width="64"
                          src="img/grocery/cart/th02.jpg"
                          alt="Product"
                        />
                      </a>
                      <div className="media-body">
                        <h6 className="widget-product-title">
                          <a href="grocery-single.html">
                            Nut Chocolate Paste (750g)
                          </a>
                        </h6>
                        <div className="widget-product-meta">
                          <span className="text-accent mr-2">
                            $6.<small>50</small>
                          </span>
                          <span className="text-muted">x 1</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="widget-cart-item py-2 border-bottom">
                    <button
                      className="close text-danger"
                      type="button"
                      aria-label="Remove"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <div className="media align-items-center">
                      <a className="d-block mr-2" href="grocery-single.html">
                        <img
                          width="64"
                          src="img/grocery/cart/th03.jpg"
                          alt="Product"
                        />
                      </a>
                      <div className="media-body">
                        <h6 className="widget-product-title">
                          <a href="grocery-single.html">
                            Mozzarella Mini Cheese
                          </a>
                        </h6>
                        <div className="widget-product-meta">
                          <span className="text-accent mr-2">
                            $3.<small>50</small>
                          </span>
                          <span className="text-muted">x 1</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="d-flex flex-wrap justify-content-between align-items-center pt-3">
                  <div className="font-size-sm mr-2 py-2">
                    <span className="text-muted">Total:</span>
                    <span className="text-accent font-size-base ml-1">
                      $25.<small>00</small>
                    </span>
                  </div>
                  <a
                    className="btn btn-primary btn-sm"
                    href="grocery-checkout.html"
                  >
                    <i className="czi-card mr-2 font-size-base align-middle"></i>
                    Checkout<i className="czi-arrow-right ml-1 mr-n1"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <!-- Search collapse--> */}
      <div className="collapse" id="searchBox">
        <div className="card pt-2 pb-4 border-0 rounded-0">
          <div className="container">
            <div className="input-group-overlay">
              <div className="input-group-prepend-overlay">
                <span className="input-group-text">
                  <i className="czi-search"></i>
                </span>
                <input
                  className="form-control prepended-form-control"
                  type="text"
                  placeholder="Search for products"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
