

function Sidebar() {
  return (
    <div >
        <aside className="cz-sidebar cz-sidebar-fixed" id="sideNav" style= {{paddingTop: "5rem" }} >
      <button className="close" type="button" data-dismiss="sidebar" aria-label="Close"><span className="d-inline-block font-size-xs font-weight-normal align-middle">Close sidebar</span><span className="d-inline-block align-middle ml-2" aria-hidden="true">&times;</span></button>
      <div className="cz-sidebar-inner">
        <ul className="nav nav-tabs nav-justified mt-2 mt-lg-4 mb-0" role="tablist" style= {{minHeight: "3rem"}} >
          <li className="nav-item"><a className="nav-link font-weight-medium active" href="#categories" data-toggle="tab" role="tab">Categories</a></li>
          <li className="nav-item"><a className="nav-link font-weight-medium" href="#menu" data-toggle="tab" role="tab">Menu</a></li>
        </ul>
        <div className="cz-sidebar-body pt-3 pb-0" data-simplebar>
          <div className="tab-content">
           
            <div className="sidebar-nav tab-pane fade show active" id="categories" role="tabpanel">
              <div className="widget widget-categories">
                <div className="accordion" id="shop-categories">
                
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#"><span className="d-flex align-items-center"><i className="czi-discount font-size-lg text-danger mt-n1 mr-2"></i>Special Offers</span></a></h3>
                    </div>
                  </div>
                 
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#bakery" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="bakery"><span className="d-flex align-items-center"><i className="czi-bread font-size-lg opacity-60 mt-n1 mr-2"></i>Bakery</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="bakery" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Bread</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Baguette</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Loaves</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Ciabatta</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Wheat bread</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Corn bread</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Rye bread</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Rye wheat bread</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Bagels</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Puff pastry</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Stuffed buns</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Buns, sweet cheese danish</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Pita and Flatbread</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                 
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#fruits" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="fruits"><span className="d-flex align-items-center"><i className="czi-apple font-size-lg opacity-60 mt-n1 mr-2"></i>Fruits and Vegetables</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="fruits" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Fruits</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Pears, apples, quinces</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Bananas, pineapples, kiwi</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Citrus</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Peaches, plums, apricots</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Grapes</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Exotic fruits</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Berries</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Vegetables</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Mushrooms</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Fresh greens</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Dried fruits</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
              
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#dairy" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="dairy"><span className="d-flex align-items-center"><i className="czi-cheese font-size-lg opacity-60 mt-n1 mr-2"></i>Dairy and Eggs</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="dairy" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Milk</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Pasteurized milk</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Condensed milk</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Sterilized milk</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Baked milk</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Powder milk</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Coconut milk</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Cheese</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Hard cheese</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Semi-hard cheese</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Pickled</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Soft cheese</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Cream-cheese</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Sour cream</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Yogurt</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Sour-milk drinks</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Butter and margarine</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Desserts</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Cream</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Eggs</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#meat" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="meat"><span className="d-flex align-items-center"><i className="czi-ham-leg font-size-lg opacity-60 mt-n1 mr-2"></i>Meat and Poultry</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="meat" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Fresh meat</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Pork</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Beef</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Rabbit</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Chicken</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Turkey</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Lamb</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Duck</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Minced meat</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Frozen ready-to-cook</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Sausages</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Meat delicatessen</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Ham</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Cold boiled pork</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Bacon</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Smoked meat</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Jamon</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Cooled meals</a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#fish" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="fish"><span className="d-flex align-items-center"><i className="czi-fish font-size-lg opacity-60 mr-2"></i>Fish and Seafood</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="fish" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Fresh fish</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Prepared fish</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Light-salted fish</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Marinated fish</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Butter with fish</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Smoked fish</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Dried fish</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Seafood</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Sushi</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#sauces" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sauces"><span className="d-flex align-items-center"><i className="czi-fish font-size-lg opacity-60 mr-2"></i>Sauces and Spices</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="sauces" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Mayonnese</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Sauces</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Cooking base</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Mustard</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Soy sauce</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Tomato paste</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Barbecue sauce</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Tartar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Hot sauces</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Balsamic sauce</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Salad dressing</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Curry sauce</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Garlic sauce</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="#">Pesto sauce</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Ketchup</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Herbs and spices</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Salt</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#canned" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="canned"><span className="d-flex align-items-center"><i className="czi-canned-food font-size-lg opacity-60 mr-2"></i>Canned Food and Oil</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="canned" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Oils and vinegar</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Canned meat</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Canned fish</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Canned fruit</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Canned vegetables</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Salads and pickles</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Olives</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Pate</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Jam, fruit paste, confiture</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Honey</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#alcoholic" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="alcoholic"><span className="d-flex align-items-center"><i className="czi-wine font-size-lg opacity-60 mr-2"></i>Alcoholic Beverages</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="alcoholic" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Beer</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Wine</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Champagne and sparkling wine</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Alcopops</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Hard liquor</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Liquor</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#drinks" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="drinks"><span className="d-flex align-items-center"><i className="czi-juice font-size-lg opacity-60 mr-2"></i>Soft Drinks and Juice</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="drinks" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Mineral water</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Juice</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Nectar</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Smoothie</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Fruit drincs</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Soda</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Tea</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Coffee</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Ice tea</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Cocoa drinks</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Hot chocolate</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Topping</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#packets" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="packets"><span className="d-flex align-items-center"><i className="czi-corn font-size-lg opacity-60 mr-2"></i>Packets, Cereals</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="packets" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Pasta</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Cereal</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Flour</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Porridge and muesli</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Snack meals</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">For baking</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Sugar and sweetener</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Soy food</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Supplements</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#frozen" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="frozen"><span className="d-flex align-items-center"><i className="czi-frozen font-size-lg opacity-60 mr-2"></i>Frozen</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="frozen" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Fish</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Meat and poultry</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Salads</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Seafood</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Pizza and breads</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Ready meals</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Fruits</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Vegetables</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Ice-cream</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Frozen bakery</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#hygiene" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="hygiene"><span className="d-flex align-items-center"><i className="czi-toothbrush font-size-lg opacity-60 mr-2"></i>Personal hygiene</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="hygiene" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Oral care</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Cotton pads</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">For ladies</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">For shaving and epilation</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Cosmetic wipes</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Soap</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#kitchenware" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="kitchenware"><span className="d-flex align-items-center"><i className="czi-pot font-size-lg opacity-60 mr-2"></i>Kitchenware</span><span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="kitchenware" data-parent="#shop-categories">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="#">View all</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Glasses, decanters</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Cookware</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Tableware</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Kitchenware</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Food storage</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="#">Disposable tableware</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="sidebar-nav tab-pane fade" id="menu" role="tabpanel">
              <div className="widget widget-categories">
                <div className="accordion" id="shop-menu">
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#home" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="home">Homepages<span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="home" data-parent="#shop-menu">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="home-fashion-store-v1.html">Fashion Store v.1</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="home-electronics-store.html">Electronics Store</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="home-marketplace.html">Multi-vendor Marketplace</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="home-grocery-store.html">Grocery Store</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="home-food-delivery.html">Food Delivery Service</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="home-fashion-store-v2.html">Fashion Store v.2</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="home-single-store.html">Single Product/Brand Store</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#shop" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="shop">Shop<span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="shop" data-parent="#shop-menu">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Shop Layouts</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-grid-ls.html">Shop Grid - Left Sidebar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-grid-rs.html">Shop Grid - Right Sidebar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-grid-ft.html">Shop Grid - Filters on Top</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-list-ls.html">Shop List - Left Sidebar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-list-rs.html">Shop List - Right Sidebar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-list-ft.html">Shop List - Filters on Top</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Marketplace</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="marketplace-category.html">Category Page</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="marketplace-single.html">Single Item Page</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="marketplace-vendor.html">Vendor Page</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="marketplace-cart.html">Cart</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="marketplace-checkout.html">Checkout</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Grocery Store</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="grocery-catalog.html">Product Catalog</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="grocery-single.html">Single Product Page</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="grocery-checkout.html">Checkout</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Food Delivery</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="food-delivery-category.html">Category Page</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="food-delivery-single.html">Single Item (Restaurant)</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="food-delivery-cart.html">Cart (Your Order)</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="food-delivery-checkout.html">Checkout (Address &amp; Payment)</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Shop pages</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-categories.html">Shop Categories</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-single-v1.html">Product Page v.1</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-single-v2.html">Product Page v.2</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="shop-cart.html">Cart</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="checkout-details.html">Checkout - Details</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="checkout-shipping.html">Checkout - Shipping</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="checkout-payment.html">Checkout - Payment</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="checkout-review.html">Checkout - Review</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="checkout-complete.html">Checkout - Complete</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="order-tracking.html">Order Tracking</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="comparison.html">Product Comparison</a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#account" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="account">Account<span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="account" data-parent="#shop-menu">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Shop User Account</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="account-orders.html">Orders History</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="account-profile.html">Profile Settings</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="account-address.html">Account Addresses</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="account-payment.html">Payment Methods</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="account-wishlist.html">Wishlist</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="account-tickets.html">My Tickets</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="account-single-ticket.html">Single Ticket</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Vendor Dashboard</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="dashboard-settings.html">Settings</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="dashboard-purchases.html">Purchases</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="dashboard-favorites.html">Favorites</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="dashboard-sales.html">Sales</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="dashboard-products.html">Products</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="dashboard-add-new-product.html">Add New Product</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="dashboard-payouts.html">Payouts</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link" href="account-signin.html">Sign In / Sign Up</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="account-password-recovery.html">Password Recovery</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#pages" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="pages">Pages<span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="pages" data-parent="#shop-menu">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="about.html">About Us</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="contacts.html">Contacts</a></li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Help Center</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="help-topics.html">Help Topics</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="help-single-topic.html">Single Topic</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="help-submit-request.html">Submit a Request</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">404 Not Found</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="404-simple.html">404 - Simple Text</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="404-illustration.html">404 - Illustration</a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#blog" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="blog">Blog<span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="blog" data-parent="#shop-menu">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Blog List Layouts</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="blog-list-sidebar.html">List with Sidebar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="blog-list.html">List no Sidebar</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Blog Grid Layouts</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="blog-grid-sidebar.html">Grid with Sidebar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="blog-grid.html">Grid no Sidebar</a></li>
                              </ul>
                            </li>
                            <li className="widget-list-item"><a className="widget-list-link font-weight-medium" href="#">Single Post Layouts</a>
                              <ul className="widget-list pt-1">
                                <li className="widget-list-item"><a className="widget-list-link" href="blog-single-sidebar.html">Article with Sidebar</a></li>
                                <li className="widget-list-item"><a className="widget-list-link" href="blog-single.html">Article no Sidebar</a></li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card border-bottom">
                    <div className="card-header">
                      <h3 className="accordion-heading font-size-base px-grid-gutter"><a className="collapsed py-3" href="#docs" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="docs">Docs / Components<span className="accordion-indicator"></span></a></h3>
                    </div>
                    <div className="collapse" id="docs" data-parent="#shop-menu">
                      <div className="card-body px-grid-gutter pb-4">
                        <div className="widget widget-links">
                          <ul className="widget-list">
                            <li className="widget-list-item"><a className="widget-list-link" href="docs/dev-setup.html">Documentation</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="components/typography.html">Components</a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="docs/changelog.html">Changelog<span className="badge badge-success ml-2">v1.4</span></a></li>
                            <li className="widget-list-item"><a className="widget-list-link" href="mailto:contact@createx.studio">Support</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="px-grid-gutter pt-5 pb-4 mb-2">
          <div className="d-flex mb-3"><i className="czi-support h4 mb-0 font-weight-normal text-primary mt-1 mr-1"></i>
            <div className="pl-2">
              <div className="text-muted font-size-sm">Support</div><a className="nav-link-style font-size-md" href="tel:+100331697720">+1 (00) 33 169 7720</a>
            </div>
          </div>
          <div className="d-flex mb-3"><i className="czi-mail h5 mb-0 font-weight-normal text-primary mt-1 mr-1"></i>
            <div className="pl-2">
              <div className="text-muted font-size-sm">Email</div><a className="nav-link-style font-size-md" href="mailto:customer@example.com">customer@example.com</a>
            </div>
          </div>
          <h6 className="pt-2 pb-1">Follow us</h6><a className="social-btn sb-outline sb-twitter mr-2 mb-2" href="#"><i className="czi-twitter"></i></a><a className="social-btn sb-outline sb-facebook mr-2 mb-2" href="#"><i className="czi-facebook"></i></a><a className="social-btn sb-outline sb-instagram mr-2 mb-2" href="#"><i className="czi-instagram"></i></a><a className="social-btn sb-outline sb-youtube mr-2 mb-2" href="#"><i className="czi-youtube"></i></a>
        </div>
      </div>
    </aside>
      </div>
      )}

      export default Sidebar;